//
//  Question.m
//  SpiceGirls
//
//  Created by Ebba on 2016-02-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "Question.h"

@implementation Question
-(instancetype)init {
    self = [super init];
    
    if(!self){
        
    }
    
    return self;
}

-(NSArray*)createQuestionList{
    
    Question *one = [[Question alloc] init];
    one.question = @"What year were Spice Girls formed?";
    one.answers = @[@"1991",@"1992",@"1993",@"1994"];
    one.correctAnswer = @"1994";
    one.beenAsked = NO;
    
    Question *two = [[Question alloc] init];
    two.question = @"What is the last name of Melanie C?";
    two.answers = @[@"Carlson",@"Chisholm",@"Carlton",@"Calliber"];
    two.correctAnswer = @"Chisholm";
    two.beenAsked = NO;
    
    Question *three = [[Question alloc] init];
    three.question = @"What was the name of Spice Girls debut single?";
    three.answers = @[@"Wannabe",@"Spice up your life",@"2 become one",@"Stop!"];
    three.correctAnswer = @"Wannabe";
    three.beenAsked = NO;
    
    Question *four = [[Question alloc] init];
    four.question = @"Approxametley, how many people applied to become a member of Spice Girls?";
    four.answers = @[@"150",@"400",@"1500",@"4000"];
    four.correctAnswer = @"400";
    four.beenAsked = NO;
    
    Question *five = [[Question alloc] init];
    five.question = @"What song did Melanie C sing at her audition to Spice Girls?";
    five.answers = @[@"I´m so exited",@"Sway",@"Mary had a little lamb",@"Stop!"];
    five.correctAnswer = @"I´m so exited";
    five.beenAsked = NO;
    
    Question *six = [[Question alloc] init];
    six.question = @"What Swede directed the video to Wannabe?";
    six.answers = @[@"Johan Camitz",@"Jonas Åkerlund",@"Jonas Karlsson",@"Gustav Dahlqvist"];
    six.correctAnswer = @"Johan Camitz";
    six.beenAsked = NO;
    
    Question *seven = [[Question alloc] init];
    seven.question = @"Whats is the name of Emma Buntons first born son?";
    seven.answers = @[@"Beau Lee Jones",@"Beau Lee Tate",@"Beau Lee Hammer",@"Gustav Dahlqvist"];
    seven.correctAnswer = @"Beau Lee Jones";
    seven.beenAsked = NO;
    
    Question *eight = [[Question alloc] init];
    eight.question = @"What was the name of the movie with Spice Girls?";
    eight.answers = @[@"Spice Girls gone bad",@"Spice World",@"Spice Sisters",@"Spice Love"];
    eight.correctAnswer = @"Spice World";
    eight.beenAsked = NO;
    
    Question *nine = [[Question alloc] init];
    nine.question = @"Who drew the Spice Bus in the movie Spice World?";
    nine.answers = @[@"David Beackham",@"Melanie B",@"Johan Camitz",@"Meat Loaf"];
    nine.correctAnswer = @"Meat Loaf";
    nine.beenAsked = NO;
    
    Question *ten = [[Question alloc] init];
    ten.question = @"What is the name of the musical with Spice Girls songs in it (premiering 2012)?";
    ten.answers = @[@"Viva Whenever: The Musical",@"Viva Forever: The Musical",@"Wannabe: The Musical",@"Wannabe: No More"];
    ten.correctAnswer = @"Viva Forever: The Musical";
    ten.beenAsked = NO;
    
    Question *eleven = [[Question alloc] init];
    eleven.question = @"Which of the Brands did NOT sponsor Spice Girls?";
    eleven.answers = @[@"Polaroid",@"PlayStation",@"Nestlé",@"Pepsi Cola"];
    eleven.correctAnswer = @"Nestlé";
    eleven.beenAsked = NO;
    
    Question *twelwe = [[Question alloc] init];
    twelwe.question = @"Which of the Brands did NOT sponsor Spice Girls?";
    twelwe.answers = @[@"Channel 5",@"Target Stores",@"Chupa Chups",@"H&M"];
    twelwe.correctAnswer = @"H&M";
    twelwe.beenAsked = NO;
    
    Question *thirteen = [[Question alloc] init];
    thirteen.question = @"What was the name of the tour with Spice Girls in 1999?";
    thirteen.answers = @[@"Christmas in Spiceworld",@"Spice World Tour",@"The return of the Spice Girls",@"Stop!"];
    thirteen.correctAnswer = @"Christmas in Spiceworld";
    thirteen.beenAsked = NO;
    
    NSArray* quizArray = @[one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelwe,thirteen];
    return quizArray;
}
@end
