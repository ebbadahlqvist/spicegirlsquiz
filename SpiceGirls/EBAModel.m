//
//  EBAModel.m
//  SpiceGirls
//
//  Created by Ebba on 2016-02-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import "EBAModel.h"

@interface EBAModel ()

@property(nonatomic) NSMutableSet* alreadyUsedQuestionsIndex;

@end


@implementation EBAModel
-(instancetype)init {
    self = [super init];
    
    if(self){
        self.listOfQuestions = [self createListWithQuestions];
        self.alreadyUsedQuestionsIndex = [[NSMutableSet alloc] init];
    }
    
    return self;
}

-(Question*)question{
    if(!_question){
        _question = [[Question alloc] init];
    }
    return _question;
}

-(Question*)pickRandomQuestion{
    
    int randomNumber = arc4random() % self.listOfQuestions.count;
    
    Question* toAsk = self.listOfQuestions[randomNumber];
    
    if([self.alreadyUsedQuestionsIndex containsObject:toAsk]){
       return[self pickRandomQuestion];
    } else {
        [self.alreadyUsedQuestionsIndex addObject:toAsk];
    }
    return toAsk;
    
}

-(NSArray*)createListWithQuestions{
    return [self.question createQuestionList];
}

-(BOOL)isAnswerCorrect:(NSString*)answer fromQuestion:(Question*)quest{
    if (answer == [quest correctAnswer]){
        return YES;
    } else {
        return NO;
    }
}

-(void)resetQuestions{
    self.alreadyUsedQuestionsIndex = [[NSMutableSet alloc]init];
}

@end

