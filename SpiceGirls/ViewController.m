//
//  ViewController.m
//  SpiceGirls
//
//  Created by Ebba on 2016-02-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//


#import "ViewController.h"
#import "EBAModel.h"
#import "Question.h"


@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (weak, nonatomic) IBOutlet UITextView *questionField;
@property (weak, nonatomic) IBOutlet UILabel *correctLabel;
@property (weak, nonatomic) IBOutlet UILabel *wrongAnswersLabel;
@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UIButton *button3;
@property (weak, nonatomic) IBOutlet UIButton *button4;
@property (strong,nonatomic) EBAModel *model;
@property (strong,nonatomic) NSString *toAsk;
@property (strong,nonatomic) NSArray *allAnswers;
@property (strong,nonatomic) NSString *correctAnswer;
@property (nonatomic)int points;
@property (nonatomic)int wrongPoints;
@property (weak, nonatomic) IBOutlet UIButton *playAgainButton;
@property (nonatomic)int answeredQuestions;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestionButton;
@property (strong,nonatomic) NSArray* allQuestions;
@property (strong, nonatomic)Question* questionInfo;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.model = [[EBAModel alloc]init];
    [self setUpQuestion];
    
}

- (IBAction)answer1:(UIButton *)sender {
   
    if([self.model isAnswerCorrect: self.button1.titleLabel.text fromQuestion:self.questionInfo]){
        [self.button1 setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [self isCorrectAnswer];
    } else {
        [self.button1 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [self wrongAnswer];
    }
}

-(void)setUpQuestion{
    
    self.questionInfo = [self.model pickRandomQuestion];
    self.toAsk = [NSString stringWithFormat:@"%@",[self.questionInfo question]];
    self.allAnswers = [self.questionInfo answers];
    self.correctAnswer = [NSString stringWithFormat:@"%@",[self.questionInfo correctAnswer]];
    
    self.questionField.text = self.toAsk;
    self.questionField.font = [UIFont systemFontOfSize:20];
    [self.button1 setTitle:self.allAnswers[0] forState:UIControlStateNormal];
    [self.button2 setTitle:self.allAnswers[1] forState:UIControlStateNormal];
    [self.button3 setTitle:self.allAnswers[2] forState:UIControlStateNormal];
    [self.button4 setTitle:self.allAnswers[3] forState:UIControlStateNormal];
    
    self.nextQuestionButton.hidden = YES;
    self.correctLabel.hidden = YES;
    self.button1.enabled = YES;
    self.button2.enabled = YES;
    self.button3.enabled = YES;
    self.button4.enabled = YES;
    [self.button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.button3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.button4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.playAgainButton.hidden = YES;

}

- (IBAction)answer2:(UIButton *)sender {
     if([self.model isAnswerCorrect: self.button2.titleLabel.text fromQuestion:self.questionInfo]){
         [self.button2 setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [self isCorrectAnswer];
    } else {
        [self.button2 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [self wrongAnswer];
    }}

- (IBAction)answer3:(UIButton *)sender {
     if([self.model isAnswerCorrect: self.button3.titleLabel.text fromQuestion:self.questionInfo]){
         [self.button3 setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [self isCorrectAnswer];
    } else {
        [self.button3 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [self wrongAnswer];
    }
}

- (IBAction)answer4:(UIButton *)sender {
     if([self.model isAnswerCorrect: self.button4.titleLabel.text fromQuestion:self.questionInfo]){
         [self.button4 setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [self isCorrectAnswer];
    } else {
        [self.button4 setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [self wrongAnswer];
    }
}
- (IBAction)nextQuestion:(UIButton *)sender {
   
    [self setUpQuestion];
}

-(void)isCorrectAnswer{
    self.correctLabel.text = @"😀";
    self.questionField.text = @"";
    self.points++;
    self.pointLabel.text = [NSString stringWithFormat:@"%d",self.points];
    self.nextQuestionButton.hidden = NO;
    self.nextQuestionButton.enabled = YES;
    self.correctLabel.hidden = NO;
    self.answeredQuestions++;
    [self enableButtons];
    if(self.answeredQuestions == 5){
        [self gameIsOver];
    }
}

-(void)wrongAnswer{
    self.correctLabel.text = @"😡";
    self.wrongPoints++;
    self.wrongAnswersLabel.text = [NSString stringWithFormat:@"%d", self.wrongPoints];
    self.questionField.text = @"";
    self.nextQuestionButton.hidden = NO;
    self.nextQuestionButton.enabled = YES;
    self.correctLabel.hidden = NO;
    self.answeredQuestions++;
    [self enableButtons];
    if(self.answeredQuestions == 5){
        [self gameIsOver];
    }
}

-(void)enableButtons{
    
    self.button1.enabled = NO;
    self.button2.enabled = NO;
    self.button3.enabled = NO;
    self.button4.enabled = NO;
}

-(void)gameIsOver{
    self.nextQuestionButton.hidden = YES;
    self.correctLabel.text = @"";
    if(self.points > 3){
        self.questionField.text = @"Congratulations! You are a true Spice Girls Fan!";
        self.questionField.font = [UIFont systemFontOfSize:20];
    } else {
        self.questionField.text = @"Ooops! You are not a Spice Girls Fan. But we know you Wannabe...";
        self.questionField.font = [UIFont systemFontOfSize:20];
    }
    
    [self enableButtons];
    self.nextQuestionButton.enabled = NO;
    self.playAgainButton.hidden = NO;
    self.playAgainButton.enabled = YES;
    
}
- (IBAction)playAgain:(UIButton *)sender {
    [self.model resetQuestions];
    self.points = 0;
    self.wrongPoints = 0;
    self.wrongAnswersLabel.text = [NSString stringWithFormat:@"%d",self.points];
    self.pointLabel.text = [NSString stringWithFormat:@"%d",self.points];
    self.answeredQuestions = 0;
    [self setUpQuestion];
}


@end
