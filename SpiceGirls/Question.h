//
//  Question.h
//  SpiceGirls
//
//  Created by Ebba on 2016-02-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question : NSObject
@property (nonatomic) NSArray* listWithQuestions;
@property (nonatomic) NSString *question;
@property (nonatomic) NSArray *answers;
@property (nonatomic) NSString* correctAnswer;
@property (nonatomic, readwrite) BOOL beenAsked;

-(NSArray*)createQuestionList;

@end
