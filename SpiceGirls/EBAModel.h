//
//  EBAModel.h
//  SpiceGirls
//
//  Created by Ebba on 2016-02-03.
//  Copyright © 2016 Ebba Dahlqvist. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Question.h"

@interface EBAModel : NSObject

@property (nonatomic) NSArray* listOfQuestions;
@property (nonatomic) Question* question;


-(BOOL)isAnswerCorrect:(NSString*)answer fromQuestion:(Question*)quest;
-(void)resetQuestions;
-(Question*)pickRandomQuestion;

@end
